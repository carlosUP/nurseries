package nurseries.personnel;

import nurseries.database.Database;
import nurseries.nursery.Nursery;
import nurseries.nursery.NurseryService;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.xml.transform.Result;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class NurseServiceTest {

    private static Database db;
    private static NurseService nurseServ;
    private static NurseryService nurseryServ;
    private static int test_schedule_id = 555;
    private static int test_schedule_id2 = 222;
    private static int test_nurse_id = -1;
    private static int test_nursery_id = -2;
    private Nurse n1;
    private Nursery nursery;

    private static Timestamp t1 = Timestamp.valueOf("2019-09-23 10:10:10.0");
    private static Timestamp t2 = Timestamp.valueOf("2019-09-24 10:10:10.0");
    private static Schedule invalid_schedule = new Schedule(test_schedule_id, t1, t2, test_nurse_id, test_nursery_id);

    private static Timestamp t5 = Timestamp.valueOf("2009-09-23 10:10:10.0");
    private static Timestamp t6 = Timestamp.valueOf("2009-09-24 10:10:10.0");
    private static Schedule invalid_schedule2 = new Schedule(12345, t1, t2, test_nurse_id, test_nursery_id);

    private static Timestamp t3 = Timestamp.valueOf("2020-09-23 10:10:10.0");
    private static Timestamp t4 = Timestamp.valueOf("2020-09-24 10:10:10.0");
    private static Schedule valid_schedule = new Schedule(test_schedule_id2, t3, t4, test_nurse_id, test_nursery_id);


    @BeforeClass
    public static void setUp() {
        db = Database.getInstance();
        db.setTestMode(true);
        nurseServ = new NurseService();
        nurseryServ = new NurseryService();

    }

    @Before
    public void prepare() {
        n1 = new Nurse(test_nurse_id);
        nursery = new Nursery(test_nursery_id, null, "Not sp", 0, null, 0);

    }

    @After
    public void clear() {
        db.execute("DELETE FROM TestNurse", true);
        db.execute("DELETE FROM TestNurseries", true);
        //db.execute("DELETE FROM Schedule", true);
        db.execute("DELETE FROM Nurse WHERE nurseId=-5");
        db.execute("DELETE FROM Nurse WHERE nurseId=-8");
        db.execute("DELETE FROM Nurse WHERE scheduleId=222");
        db.execute("DELETE FROM Schedule WHERE scheduleId=222");
    }

    @Test
    public void addNurse() throws SQLException {
        assertTrue(nurseServ.addNurse(n1).response);

    }


   /* @Test
    public void addNurseToNursery() throws SQLException {
        Nurse n2= new Nurse(-5,0);

        assertTrue(nurseServ.addNurse(n2).response);

        assertTrue(nurseryServ.addNursery(nursery).response);
        //assertFalse(nurseServ.addNurseToNursery(nursery, n1, invalid_schedule).response);
        assertTrue(nurseServ.addNurseToNursery(nursery, n2, invalid_schedule2).response);

        ArrayList<Nurse> nurses = (ArrayList<Nurse>) nurseServ.getNursesInNursery(nursery.id).response;
        assertTrue(nurses.contains(n2));
       // assertEquals(nurses.get(0).id, n1.id);
       // assertEquals(nurses.get(0).scheduleId, test_schedule_id2);
    }*/


    @Test
    public void getNursesInNursery() throws SQLException {
        assertTrue(nurseServ.addNurse(n1).response);
        assertTrue(nurseryServ.addNursery(nursery).response);

        Nurse n2= new Nurse(-8,0);
        assertTrue(nurseServ.addNurse(n2).response);

        assertFalse(nurseServ.addNurseToNursery(nursery, n2, invalid_schedule2).response);


        Schedule s3 = new Schedule(999, Timestamp.valueOf("2020-09-04 10:10:10.0"), Timestamp.valueOf("2020-09-05 10:10:10.0"), n1.id, nursery.id);
        assertFalse(nurseServ.addNurseToNursery(nursery, n1, s3).response);

        ArrayList<Nurse> nurses = (ArrayList<Nurse>) nurseServ.getNursesInNursery(nursery.id).response;
        //assertTrue(nurses.contains(n2));
        assertFalse(nurses.contains(n1));



    }


   /* @Test
    public void getNurseSchedule() throws SQLException {

        assertTrue(nurseServ.addNurse(n1).response);

        Nurse n2= new Nurse(-8,0);
        assertTrue(nurseServ.addNurse(n2).response);

        assertTrue(nurseryServ.addNursery(nursery).response);
        assertTrue(nurseServ.addNurseToNursery(nursery, n2, valid_schedule).response);


        ResultSet rs = db.getConnection().createStatement().executeQuery("SELECT * from Schedule WHERE nurseId=222");

        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();

        Schedule s1 = nurseServ.getNurseSchedule(n1.id).response;

        assertTrue(nurseServ.getNurseSchedule(n1.id).response.equals(valid_schedule));
    }*/

    @Test
    public void getNurse() {

        assertTrue(nurseServ.addNurse(n1).response);
        Nurse n2 = new Nurse(2, 0, "test2");
        assertTrue(nurseServ.addNurse(n2).response);

        assertEquals(nurseServ.getNurse(n2.id, n2.idCard).response, n2);

        Nurse n = nurseServ.getNurse(n1.id, n1.idCard).response;

        assertEquals(nurseServ.getNurse(n1.id, n1.idCard).response, n1);


    }


    /*@Test
    public void getAvailableNurses() {
        assertTrue(nurseServ.addNurse(n1).response);

        assertTrue(nurseryServ.addNursery(nursery).response);
        assertTrue(nurseServ.addNurseToNursery(nursery, n1, valid_schedule).response);

        Nurse n2 = new Nurse(2);

        long tomorrowMinusSevenHours = new Timestamp(System.currentTimeMillis()).getTime() + 30200000;
        long tomorrowMinusFifteenHours = new Timestamp(System.currentTimeMillis()).getTime() + 25200000;

        Schedule s3 = new Schedule(1, new Timestamp(tomorrowMinusFifteenHours), new Timestamp(tomorrowMinusSevenHours), 2, -1);

        assertTrue(nurseServ.addNurse(n2).response);
        assertTrue(nurseServ.addNurseToNursery(nursery, n2, s3).response);

        Nurse n3 = new Nurse(3);
        nurseServ.addNurse(n3);

        ArrayList<Nurse> availableNurses = (ArrayList<Nurse>) nurseServ.getAvailableNurses().response;

        assertTrue(availableNurses.get(0).equals(n3));
        assertFalse(availableNurses.contains(n2));



    }*/

    @Test
    public void nurseExists() {
        n1.idCard = "test";
        System.out.println(nurseServ.addNurse(n1).errorMessage);
        assertTrue(nurseServ.addNurse(n1).response);
        Nurse n2 = new Nurse(-5, 0);
        assertTrue(nurseServ.nurseExists(n1.id, n1.idCard).response);
        assertFalse(nurseServ.nurseExists(n2.id, null).response);

    }

  /*  @Test
    public void getSchedule() {
        assertTrue(nurseServ.addSchedule(valid_schedule).response);
        assertEquals(nurseServ.getSchedule(valid_schedule.id).response, valid_schedule);

    }*/

  /*  @Test
    public void addSchedule() {
        assertTrue(nurseServ.addSchedule(valid_schedule).response);
        assertEquals(nurseServ.getSchedule(valid_schedule.id).response, valid_schedule);
    }*/

    @Test
    public void scheduleExists() {
        assertTrue(nurseServ.addSchedule(valid_schedule).response);
        assertTrue(nurseServ.scheduleExists(valid_schedule.id).response);
        assertFalse(nurseServ.scheduleExists(invalid_schedule.id).response);
    }
}


