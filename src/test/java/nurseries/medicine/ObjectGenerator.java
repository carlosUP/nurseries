package nurseries.medicine;

import nurseries.database.Database;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ObjectGenerator {

    private Database db;

    public ObjectGenerator(Database db) {
        this.db = db;
    }

    public boolean makeTestNursery(int id) throws SQLException {
        String sql = "INSERT INTO TestNurseries VALUES(?, ?, ?, ?, ?, ?)";
        PreparedStatement s = db.getConnection().prepareStatement(sql);
        s.setInt(1, id);
        s.setString(2, "test");
        s.setString(3, "-");
        s.setInt(4, 0);
        s.setString(5, "-");
        s.setInt(6, 0);
        return s.executeUpdate() == 1;
    }

    public boolean deleteTestNursery(int id) throws SQLException {
        String sql = "DELETE FROM TestNurseries WHERE id = ?";
        PreparedStatement s = db.getConnection().prepareStatement(sql);
        s.setInt(1, id);
        return s.executeUpdate() == 1;
    }
}
