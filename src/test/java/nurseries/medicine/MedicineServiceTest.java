package nurseries.medicine;

import nurseries.database.Database;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;

public class MedicineServiceTest {

    private static Database db;
    private static MedicineService serv;
    private static ObjectGenerator gen;
    private static int test_id = 0;
    private static int test_id2 = -1;
    private static int no_exist_id = -2;

    @BeforeClass
    public static void setUp() throws SQLException {
        db = Database.getInstance();
        db.setTestMode(true);
        serv = new MedicineService("localhost",9092);
        gen = new ObjectGenerator(db);
        gen.makeTestNursery(test_id);
    }

    @AfterClass
    public static void tearDown() throws SQLException {
        gen.deleteTestNursery(test_id);
    }

    @After
    public void clear() {
        db.execute("DELETE FROM TestMedicineStock", true);
    }

    @Test
    public void emptyStock() {
        assertEquals(serv.getStock(test_id).response.size(),0);
    }

    @Test
    public void addRemoveStock() {
        serv.add(test_id, test_id, 2);
        assertEquals(serv.getStock(test_id).response.size(),1);
        serv.remove(test_id, test_id, 2);
        List<Medicine> meds = serv.getStock(test_id).response;
        assertEquals(meds.size(),1);
        assertEquals(meds.get(0).stock, 0);
    }

    @Test
    public void addRemoveSomeStock() {
        serv.add(test_id, test_id, 5);
        assertEquals(serv.getStock(test_id).response.size(),1);
        serv.remove(test_id, test_id, 2);
        List<Medicine> meds = serv.getStock(test_id).response;
        assertEquals(meds.size(),1);
        assertEquals(meds.get(0).stock, 3);
    }

    @Test
    public void addMoreStock() {
        serv.add(test_id, test_id, 2);
        assertEquals(serv.getStock(test_id).response.size(),1);
        serv.add(test_id, test_id, 5);
        List<Medicine> meds = serv.getStock(test_id).response;
        assertEquals(meds.size(),1);
        assertEquals(meds.get(0).stock,7);
    }

    @Test
    public void addDifferentStock() {
        serv.add(test_id, test_id, 2);
        serv.add(test_id, test_id2, 5);
        assertEquals(serv.getStock(test_id).response.size(),2);
    }

    @Test
    public void badRemove() {
        assertTrue(serv.remove(test_id,test_id,0).error);
    }

    @Test
    public void tooManyRemoved() {
        serv.add(test_id, test_id, 2);
        assertEquals(serv.getStock(test_id).response.size(),1);
        assertFalse(serv.remove(test_id, test_id, 4).response);
    }

    @Test
    public void uselessRemove() {
        assertTrue(serv.remove(test_id,no_exist_id,0).error);
        assertTrue(serv.remove(no_exist_id,test_id,0).error);
        assertTrue(serv.remove(no_exist_id,no_exist_id,0).error);
    }

    @Test
    public void badAdd() {
        assertTrue(serv.add(test_id,test_id,0).error);
    }

    @Test
    public void badGet() {
        assertTrue(db.getMedicine(test_id,no_exist_id).error);
    }

    @Test
    public void badFind() {
        assertFalse(db.medicineExists(test_id, no_exist_id).response);
    }

    @Test
    public void goodFind() {
        serv.add(test_id, test_id, 2);
        assertTrue(db.medicineExists(test_id, test_id).response);
    }

}