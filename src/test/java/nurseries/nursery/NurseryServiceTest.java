package nurseries.nursery;

import nurseries.database.Database;
import org.junit.*;
import static org.junit.Assert.*;

public class NurseryServiceTest {

    private static Database db;
    private static NurseryService serv;
    private static int test_id = 0;
    private static int test_id2 = -1;
    private static int test_id3 = -2;
    private static int no_exist_id = -3;
    private Nursery ns1;
    private Nursery ns2;
    private Nursery ns3;

    @BeforeClass
    public static void setUp() {
        db = Database.getInstance();
        db.setTestMode(true);
        serv = new NurseryService();
    }

    @Before
    public void prepare() {
        ns1 = new Nursery(test_id, "A", "Sp", 0, "-", 0);
        ns2 = new Nursery(test_id2, null, "Not sp", 0, null, 0);
        ns3 = new Nursery(test_id3, "B", "Sp", 0, "-", 0);
    }

    @After
    public void clear() {
        db.execute("DELETE FROM TestNurseries", true);
    }

    @Test
    public void emptyNurseries() {
        assertEquals(serv.getNurseries().response.size(),0);
    }

    @Test
    public void addNursery() {
        assertTrue(serv.addNursery(ns1).response);
        assertEquals(serv.getNurseries().response.size(),1);
        assertTrue(serv.addNursery(ns3).response);
        assertEquals(serv.getNurseries().response.size(),2);
    }

    @Test
    public void removeNursery() {
        assertTrue(serv.addNursery(ns1).response);
        assertEquals(serv.getNurseries().response.size(),1);
        assertTrue(serv.removeNursery(ns1.id).response);
        assertEquals(serv.getNurseries().response.size(),0);
    }

    @Test
    public void uselessRemove() {
        assertFalse(serv.removeNursery(no_exist_id).response);
    }

    @Test
    public void addNullFieldNursery() {
        assertTrue(serv.addNursery(ns2).response);
        assertEquals(serv.getNurseries().response.size(),1);
    }

    @Test
    public void getNursery() {
        assertTrue(serv.addNursery(ns1).response);
        assertEquals(serv.getNursery(ns1.id).response.id, test_id);
    }

    @Test
    public void badGet() {
        assertTrue(serv.getNursery(no_exist_id).error);
    }

    @Test
    public void badExists() {
        assertFalse(db.nurseryExists(no_exist_id).response);
    }

    @Test
    public void goodExists() {
        assertTrue(serv.addNursery(ns1).response);
        assertTrue(db.nurseryExists(ns1.id).response);
    }

    @Test
    public void types() {
        assertTrue(serv.addNursery(ns1).response);
        assertEquals(serv.getTypes().response.size(),1);
        assertTrue(serv.addNursery(ns2).response);
        assertEquals(serv.getTypes().response.size(),2);
        assertTrue(serv.addNursery(ns3).response);
        assertEquals(serv.getTypes().response.size(),2);
    }

    @Test
    public void goodEdit() {
        assertTrue(serv.addNursery(ns1).response);
        Nursery n = serv.getNursery(ns1.id).response;
        assertEquals(n.id,test_id);
        assertEquals(n.name,ns1.name);
        assertEquals(n.location,ns1.location);
        ns1.name = ns3.name;
        assertTrue(serv.editNursery(ns1).response);
        n = serv.getNursery(ns1.id).response;
        assertEquals(n.id,test_id);
        assertEquals(n.name,ns3.name);
        assertEquals(n.location,ns1.location);
    }

    @Test
    public void badEdit() {
        ns1.id = no_exist_id;
        assertFalse(serv.editNursery(ns1).response);
    }

}