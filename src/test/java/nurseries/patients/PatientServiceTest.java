package nurseries.patients;

import nurseries.database.Database;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;

public class PatientServiceTest {

    private static Database db;
    private static PatientService serv;
    private static ObjectGenerator gen;
    private static int test_id = 0;
    private static int test_id2 = -2;
    private static int test_id3 = -3;
    private static int no_exist_id = -4;

    @BeforeClass
    public static void setUp() throws SQLException {
        db = Database.getInstance();
        db.setTestMode(true);
        serv = new PatientService();
        gen = new ObjectGenerator(db);
        gen.makeTestNursery(test_id, "A", 1);
        gen.makeTestNursery(test_id2, "A", 100);
        gen.makeTestNursery(test_id3, "B", 1);
    }

    @AfterClass
    public static void tearDown() throws SQLException {
        gen.deleteTestNursery(test_id);
        gen.deleteTestNursery(test_id2);
        gen.deleteTestNursery(test_id3);
    }

    @After
    public void clear() {
        db.execute("DELETE FROM TestPatients", true);
    }

    @Test
    public void emptyPatients() {
        assertEquals(serv.getPatients(test_id).response.size(),0);
    }

    @Test
    public void addPatientA() {
        assertEquals(serv.allocatePatient(test_id, "A").response.intValue(),test_id);
        List<Patient> pats = serv.getPatients(test_id).response;
        assertEquals(pats.size(),1);
        assertEquals(pats.get(0).id,test_id);
        assertEquals(pats.get(0).nurseryId,test_id);
    }

    @Test
    public void addPatientFind() {
        assertEquals(serv.allocatePatient(test_id, "A").response.intValue(),test_id);
        assertEquals(serv.allocatePatient(test_id2, "A").response.intValue(),test_id2);
    }

    @Test
    public void addPatientOverflow() {
        assertEquals(serv.allocatePatient(test_id, "B").response.intValue(),test_id3);
        assertEquals(serv.allocatePatient(test_id2, "B").response.intValue(),-1);
    }

    @Test
    public void badAdd() {
        assertEquals(serv.allocatePatient(test_id, "Z").response.intValue(),-1);
    }

    @Test
    public void find() {
        assertEquals(db.findAvailableNursery("A").response.intValue(),test_id);
        serv.allocatePatient(test_id,"A");
        assertEquals(db.findAvailableNursery("A").response.intValue(),test_id2);
    }

    @Test
    public void badFind() {
        assertEquals(db.findAvailableNursery("C").response.intValue(),-1);
    }

    @Test
    public void countA() {
        serv.allocatePatient(test_id, "A");
        assertEquals(db.getPatientCount(test_id).response.intValue(),1);
        serv.allocatePatient(test_id2, "A");
        serv.allocatePatient(test_id3, "A");;
        assertEquals(db.getPatientCount(test_id2).response.intValue(),2);
    }

    @Test
    public void removePatient() {
        serv.allocatePatient(test_id, "A");
        assertEquals(db.getPatientCount(test_id).response.intValue(),1);
        serv.dischargePatient(test_id);
        assertEquals(db.getPatientCount(test_id).response.intValue(),0);
    }

    @Test
    public void badRemove() {
        assertFalse(db.removePatient(no_exist_id).response);
    }

    @Test
    public void getPatient() {
        assertEquals(serv.allocatePatient(test_id, "A").response.intValue(),test_id);
        assertEquals(serv.getPatient(test_id).response.nurseryId,test_id);
    }

    @Test
    public void badGet() {
        assertTrue(serv.getPatient(no_exist_id).error);
    }

    @Test
    public void badExists() {
        assertFalse(db.patientExists(no_exist_id).response);
    }

    @Test
    public void goodExists() {
        assertEquals(serv.allocatePatient(test_id, "A").response.intValue(),test_id);
        assertTrue(db.patientExists(test_id).response);
    }

}