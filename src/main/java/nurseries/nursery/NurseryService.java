package nurseries.nursery;

import nurseries.database.Database;
import nurseries.database.DatabaseResponse;
import nurseries.personnel.Nurse;
import nurseries.personnel.Schedule;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

public class NurseryService {

    private Database db;

    public NurseryService() {
        this.db = Database.getInstance();
    }

    public DatabaseResponse<List<Nursery>> getNurseries() {
        return db.getNurseries();
    }

    public DatabaseResponse<Nursery> getNursery(int nurseryId) {
        return db.getNursery(nurseryId);
    }

    public DatabaseResponse<Boolean> nurseryExists(int nurseryId) {
        return db.nurseryExists(nurseryId);
    }

    //all types currently in use
    public DatabaseResponse<Set<String>> getTypes() {
        return db.getNurseryTypes();
    }

    public DatabaseResponse<Boolean> removeNursery(int nurseryId) {
        return db.removeNursery(nurseryId);
    }

    public DatabaseResponse<Boolean> addNursery(Nursery nursery) {
        return db.addNursery(nursery);
    }

   public DatabaseResponse<Boolean> editNursery(Nursery nursery) {
        return db.editNursery(nursery);
    }

}
