package nurseries.nursery;

import org.json.JSONObject;

public class Nursery {
    public int id;
    public String name;
    public String specialty;
    public int beds;
    public String location;
    public int responsible;

    public Nursery(int id, String name, String specialty, int beds, String location, int responsible) {
        this.id = id;
        this.name = name;
        this.specialty = specialty;
        this.beds = beds;
        this.location = location;
        this.responsible = responsible;
    }

    public JSONObject toJSON() {
        return new JSONObject().put("id", id).put("name", name).put("type", specialty)
                .put("beds", beds).put("location", location).put("responsible", responsible);
    }

    @Override
    public String toString() {
        return String.format("(nurseryId: %d, name: %s, specialty: %s, beds: %d, location: %s, responsible: %d)",
                id, name, specialty, beds, location, responsible);
    }
}
