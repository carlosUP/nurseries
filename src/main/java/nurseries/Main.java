package nurseries;

import nurseries.database.Database;
import nurseries.spring.Api;
import nurseries.spring.ErrorHandler;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import java.util.HashMap;

@SpringBootApplication
public class Main {

	public static void main(String[] args) {

		String[] topics = {"allocate", "discharge"};
		//Listener.start("localhost", 9092, topics, 1);

		HashMap<String, Object> properties = new HashMap<>();
		properties.put("server.port", 8081);

		new SpringApplicationBuilder().properties(properties).sources(Api.class, ErrorHandler.class).run(args);

	}

}
