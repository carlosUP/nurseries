package nurseries.kafka;

import java.util.Timer;
import java.util.TimerTask;
import nurseries.kafka.Consumer.Message;

public class Listener extends TimerTask {

    private static Timer time;
    private static Listener poll;
    private Consumer consumer;
    private Producer producer;
    private Replier replier;

    private Listener(String host, int port, String[] topics) {
        consumer = new Consumer(host, port, topics);
        producer = new Producer(host, port);
        replier = new Replier();
    };

    public static void start(String host, int port, String[] topics, long millis) {
        time = new Timer();
        poll = new Listener(host, port, topics);
        time.schedule(poll, 0, millis);
    }

    public static void end() {
        time.cancel();
        poll.consumer.close();
        poll.producer.close();
    }

    @Override
    public void run() {
        for (Message msg: consumer.receive())
            producer.send(msg.topic+"-reply", replier.formReply(msg));
    }

}
