package nurseries.kafka;

import java.util.Properties;
import java.util.concurrent.Future;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

public class Producer {

	private KafkaProducer<String,String> producer;
	private final String DEFAULT_HOST = "localhost";
	private final int DEFAULT_PORT = 9092;
	
	public Producer() {
		init(DEFAULT_HOST, DEFAULT_PORT);
	}
	
	public Producer(String host, int port) {
		init(host, port);
	}
	
	private void init(String host, int port) {
		Properties props = new Properties();
	    props.put("bootstrap.servers", host+":"+port);
	    props.put("acks", "all");
	    props.put("retries", 0);
	    props.put("batch.size", 16384);
	    props.put("linger.ms", 1);
	    props.put("buffer.memory", 33554432);
	    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
	    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
	    //producer = new KafkaProducer<>(props);
	}
	
	public Future<RecordMetadata> send(String topic, Object message) {
		return producer.send(new ProducerRecord<>(topic, message.toString()));
	}
	
	public void close() {
		producer.close();
	}
}
