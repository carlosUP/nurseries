package nurseries.kafka;

import nurseries.kafka.Consumer.Message;
import nurseries.patients.PatientService;
import org.json.JSONObject;

public class Replier {

    private PatientService patients;

    public Replier() {
        patients = new PatientService();
    }

    public JSONObject formReply(Message msg) {
        switch (msg.topic) {
            case "allocate":
                return allocate(msg);
            case "discharge":
                return discharge(msg);
            default:
                return error("unrecognised topic");
        }
    }

    private JSONObject allocate(Message msg) {
        //patients.allocatePatient(   )
        return null;
    }

    private JSONObject discharge(Message msg) {
        //patients.dischargePatient(   )
        return null;
    }

    private JSONObject error(String error) {
        return new JSONObject().put("error", error);
    }
}
