package nurseries.kafka;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;

public class Consumer {
	
	private KafkaConsumer<String, String> consumer;
	private final String DEFAULT_HOST = "localhost";
	private final int DEFAULT_PORT = 9092;
	
	public Consumer() {
		init(DEFAULT_HOST, DEFAULT_PORT);
	}
	
	public Consumer(String... topics) {
		init(DEFAULT_HOST, DEFAULT_PORT);
		subscribe(topics);
	}
	
	public Consumer(String host, int port) {
		init(host, port);
	}
	
	public Consumer(String host, int port, String... topics) {
		init(host, port);
		subscribe(topics);
	}
	
	private void init(String host, int port) {
		Properties props = new Properties();
	    props.put("bootstrap.servers", host+":"+port);
	    props.put("group.id", "group-1");
	    props.put("enable.auto.commit", "true");
	    props.put("auto.commit.interval.ms", "1000");
	    props.put("auto.offset.reset", "earliest");
	    props.put("session.timeout.ms", "30000");
	    props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
	    props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
	    //consumer = new KafkaConsumer<>(props);
	}
	
	public void subscribe(String... topics) {
		consumer.subscribe(Arrays.asList(topics));
	}
	
	public ArrayList<Message> receive() {
		ArrayList<Message> messages = new ArrayList<>();

		for (ConsumerRecord<String,String> record: consumer.poll(100))
			messages.add(new Message(record));

		return messages;
	}

	public void close() {
		consumer.close();
	}

	public class Message {
		public String topic;
		public String key;
		public String value;

		public Message(ConsumerRecord<String,String> record) {
			topic = record.topic();
			key = record.key();
			value = record.value();
		}
	}
    
}
