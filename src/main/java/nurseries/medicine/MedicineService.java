package nurseries.medicine;

import nurseries.database.*;
import nurseries.kafka.*;
import nurseries.spring.Exceptions.*;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.json.JSONObject;
import java.util.List;
import java.util.concurrent.Future;

public class MedicineService {

    private Database db;
    private Producer prod;
    public final String requestTopic = "medicine-request";

    public MedicineService(String host, int port) {
        this.db = Database.getInstance();
        this.prod = new Producer(host, port);
    }

    public DatabaseResponse<List<Medicine>> getStock(int nurseryId) {
        return db.getStock(nurseryId);
    }

    public DatabaseResponse<Boolean> medicineExists(int nurseryId, int medicineId) {
        return db.medicineExists(nurseryId, medicineId);
    }

    public DatabaseResponse<Medicine> getMedicine(int nurseryId, int medicineId) {
        return db.getMedicine(nurseryId, medicineId);
    }

    public Future<RecordMetadata> request(int nurseryId, int medicineId, int amount) {
        try {
            JSONObject json = new JSONObject();
            json.put("nurseryId", nurseryId).put("medicineId", medicineId).put("amount", amount);
            return prod.send(requestTopic, json.toString());
        } catch (Exception e) {
            throw new KafkaException(requestTopic+": "+e.getMessage());
        }
    }

    public DatabaseResponse<Boolean> add(int nurseryId, int medicineId, int amount) {
        return db.addStock(nurseryId, medicineId, amount);
    }

    public DatabaseResponse<Boolean> remove(int nurseryId, int medicineId, int amount) {
        return db.consumeStock(nurseryId, medicineId, amount);
    }
}
