package nurseries.medicine;

import org.json.JSONObject;

public class Medicine {
    public int id;
    public int stock;

    public Medicine(int id, int stock) {
        this.id = id;
        this.stock = stock;
    }

    public JSONObject toJSON() {
        return new JSONObject().put("id", id).put("stock", stock);
    }

    @Override
    public String toString() {
        return String.format("(medicineId: %d, stock: %d)", id, stock);
    }
}
