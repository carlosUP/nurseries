package nurseries.personnel;

import java.sql.Timestamp;
import nurseries.spring.Exceptions.*;
import java.util.Calendar;
import java.util.Date;

import static nurseries.spring.Utils.hours;
import static nurseries.spring.Utils.minutes;

public class Schedule {
    public int id;
    public int nurseryId;
    public int nurseId;
    public Timestamp timeStart;
    public Timestamp timeExit;

    public Schedule(int id, String type, int nurseId, int nurseryId) {
        this.id = id;
        this.nurseryId = nurseryId;
        this.nurseId = nurseId;

        switch (type.toLowerCase()) {
            case "morning":
                this.timeStart = new Timestamp(hours(6));
                this.timeExit = new Timestamp(hours(14));
                break;
            case "afternoon":
                this.timeStart = new Timestamp(hours(14));
                this.timeExit = new Timestamp(hours(22));
                break;
            case "night":
                this.timeStart = new Timestamp(hours(22));
                this.timeExit = new Timestamp(hours(06));
                break;
            default:
                throw new ScheduleException("type '"+type+"' is unrecognised");
        }
    }

    public Schedule(int id, Timestamp timeStart, Timestamp timeExit, int nurseId, int nurseryId) {
        if (timeExit.before(timeStart)) throw new ScheduleException("start time is later than exit time");

        this.id = id;
        this.nurseryId = nurseryId;
        this.nurseId = nurseId;
        this.timeStart = timeStart;
        this.timeExit = timeExit;
    }

    @Override
    public boolean equals(Object object) {
        try {
            Schedule secondSchedule = (Schedule) object;
            return this.id == secondSchedule.id && this.timeStart.equals(secondSchedule.timeStart) &&
                    this.timeExit.equals(secondSchedule.timeExit) && this.nurseId == secondSchedule.nurseId &&
                    this.nurseryId == secondSchedule.nurseryId;
        } catch (ClassCastException e) {
            return false;
        }
    }

    public long currentMillisAfterExit() {
        Calendar calendar = Calendar.getInstance();
        long now = hours(calendar.get(Calendar.HOUR_OF_DAY)) + minutes(calendar.get(Calendar.MINUTE));
        return now-timeExit.getTime()<0 ? now+hours(24)-timeExit.getTime() : now-timeExit.getTime();
    }

}
