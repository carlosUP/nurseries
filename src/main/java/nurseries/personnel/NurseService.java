package nurseries.personnel;

import nurseries.database.Database;
import nurseries.database.DatabaseResponse;
import nurseries.nursery.Nursery;
import org.springframework.lang.Nullable;

import java.sql.Timestamp;
import java.time.DateTimeException;
import java.util.List;

public class NurseService {

    private Database db;

    public NurseService() {
        this.db = Database.getInstance();
    }

    public DatabaseResponse<Boolean> addNurse(Nurse nurse) {
        return db.addNurse(nurse);
    }

    public DatabaseResponse<List<Nurse>> getNursesInNursery(int nurseryId) {
        return db.getNursesInNursery(nurseryId);
    }

    public DatabaseResponse<Nurse> getNurse(int nurseId, @Nullable String idCard) {
        return db.getNurse(nurseId,idCard);
    }

    public DatabaseResponse<Boolean> nurseExists(int nurseId,String idCard){

        return db.nurseExists(nurseId,idCard);
    }

    public DatabaseResponse<Schedule> getNurseSchedule(int nurseId) {
        return db.getNurseSchedule(nurseId);
    }

    public DatabaseResponse<Boolean> addNurseToNursery(Nursery nursery, Nurse nurse, Schedule schedule) {

        //se ele tem um schedule válido nunca poderá pertencer a outra nursery

        boolean nurseIsFree = true;

        DatabaseResponse<Boolean> falseResponse = new DatabaseResponse<>();
        falseResponse.response = false;


        DatabaseResponse<Schedule> s = getNurseSchedule(nurse.id);

        if (s.error == false || s == null) {
            if (s.response.timeExit.before(new Timestamp(System.currentTimeMillis()))) {
                nurseIsFree = true;
            } else {
                nurseIsFree = false;
            }
        }
        if (schedule.timeStart.before(new Timestamp(System.currentTimeMillis()))
                || schedule.timeExit.before(new Timestamp(System.currentTimeMillis()))) {

            return falseResponse;
        }

        if (nurseIsFree) {

            nurse.scheduleId = schedule.id;
            return db.addNurseToNursery(nursery, nurse, schedule);
        } else {

            return falseResponse;
        }
    }

    public DatabaseResponse<List<Nurse>> getAvailableNurses (){
        return db.getAvailableNurses();
    }

    public DatabaseResponse<Schedule> getSchedule(int scheduleId){return db.getSchedule(scheduleId);}

    public DatabaseResponse<Boolean> addSchedule(Schedule schedule){return db.addSchedule(schedule);}

    public DatabaseResponse<Boolean> scheduleExists(int scheduleId){return db.scheduleExists(scheduleId);}

}
