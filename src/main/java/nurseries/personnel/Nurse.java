package nurseries.personnel;

import org.json.JSONObject;

public class Nurse {
    public int id;
    public int scheduleId;
    public String idCard;

    public Nurse(int id, int scheduleId) {
        this.id = id;
        this.scheduleId = scheduleId;
    }

    public Nurse(int nurseId) {
        this.id = nurseId;
        this.scheduleId = 0;
    }

    public Nurse(int nurseId,int scheduleId, String idCard){
        this.id = id;
        this.scheduleId = scheduleId;
        this.idCard=idCard;
    }

    public JSONObject toJSON() {
        if(idCard==null) {
            return new JSONObject().put("idCard", id).put("scheduleId", scheduleId);
        }else{
            return new JSONObject().put("idCard", idCard).put("scheduleId", scheduleId);
        }
    }

    @Override
    public String toString() {
        return String.format("(NurseID: %d, ScheduleID: %d,IdCard: %d)", id, scheduleId,idCard);
    }

    @Override
    public boolean equals(Object object) {
        try {
            Nurse nurse2 = (Nurse) object;
            return this.scheduleId == nurse2.scheduleId && this.id == nurse2.id;
        } catch (ClassCastException e) {
            return false;
        }
    }
}
