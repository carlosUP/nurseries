package nurseries.database;

import nurseries.spring.Exceptions.DatabaseException;
import java.sql.SQLException;

public class DatabaseResponse<T> {
    public boolean error;
    public String errorMessage;
    public T response;

    public DatabaseResponse<T> ok(T response) {
        this.error = false;
        this.response = response;
        return this;
    }

    public DatabaseResponse<T> error(String errorMessage) {
        this.error = true;
        this.errorMessage = errorMessage;
        return this;
    }

    public DatabaseResponse<T> error(SQLException ex) {
        this.error = true;
        this.errorMessage = ex.getMessage();
        return this;
    }

    /**
     * Throws a DatabaseException if the response contains an error. Returns the response otherwise.
     */
    public T get() { if (error) throw new DatabaseException(errorMessage); else return response; }

    @Override
    public String toString() {
        if (error) return String.format("DatabaseResponse: (error: true, message: \"%s\")", errorMessage);
        else return String.format("DatabaseResponse: (error: false, response: %s)", response.toString());
    }
}
