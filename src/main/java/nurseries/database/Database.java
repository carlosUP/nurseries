package nurseries.database;

import nurseries.medicine.Medicine;
import nurseries.nursery.Nursery;
import nurseries.patients.Patient;
import nurseries.personnel.Nurse;
import nurseries.personnel.Schedule;
import org.springframework.lang.Nullable;

import java.sql.*;
import java.util.*;

/**
 * Direct database connection,
 * low-level SQL queries
 * and object creation.
 */
public class Database {

    private final String HOST = "nurserydb.cntfluvyg3rc.eu-west-2.rds.amazonaws.com";
    private final String PORT = "3306";
    private final String USER = "admin";
    private final String PASSWORD = "qwerty123";
    private Connection con;
    private boolean testMode = false;
    private static Database instance;

    public static Database getInstance() {
        if (instance == null) instance = new Database();
        return instance;
    }

    private Database() {
        getConnection();
    }

    public Connection getConnection() {
        if (con == null) con = createConnection();
        return con;
    }

    public void closeConnection() {
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Connection createConnection() {
        Connection conn = null;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://" + HOST + ":" + PORT + "/mydb", USER, PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return conn;
    }

    public void setTestMode(boolean b) {
        testMode = b;
    }

    private String tableName(String table) {
        return testMode ? "Test"+table : table;
    }

    public DatabaseResponse<List<Medicine>> getStock(int nurseryId) {
        DatabaseResponse<List<Medicine>> db = new DatabaseResponse<>();

        try {
            ArrayList<Medicine> meds = new ArrayList<>();

            String sql = "SELECT * FROM "+tableName("MedicineStock")+" WHERE nurseryId = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, nurseryId);
            ResultSet rs = s.executeQuery();

            while (rs.next())
                meds.add(new Medicine(rs.getInt(2), rs.getInt(3)));

            return db.ok(meds);
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Boolean> consumeStock(int nurseryId, int medicineId, int amount) {
        DatabaseResponse<Boolean> db = new DatabaseResponse<>();
        if (amount < 1) return db.error("Amount must be positive.");

        try {
            DatabaseResponse<Medicine> m = getMedicine(nurseryId, medicineId);
            if (m.error) return db.error(m.errorMessage);
            if (m.response.stock < amount) return db.ok(false);

            String sql = "UPDATE "+tableName("MedicineStock")+" SET stock = ? WHERE nurseryId = ? AND medicineId = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, m.response.stock - amount);
            s.setInt(2, nurseryId);
            s.setInt(3, medicineId);
            return db.ok(s.executeUpdate() == 1);
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Boolean> addStock(int nurseryId, int medicineId, int amount) {
        DatabaseResponse<Boolean> db = new DatabaseResponse<>();
        if (amount < 1) return db.error("Amount must be positive.");

        try {
            DatabaseResponse<Boolean> exists = medicineExists(nurseryId, medicineId);
            if (exists.error) return db.error(exists.errorMessage);

            if (exists.response) { //update
                DatabaseResponse<Medicine> m = getMedicine(nurseryId, medicineId);
                if (m.error) return db.error(m.errorMessage);

                String sql = "UPDATE "+tableName("MedicineStock")+" SET stock = ? WHERE nurseryId = ? AND medicineId = ?";
                PreparedStatement s = con.prepareStatement(sql);
                s.setInt(1, m.response.stock + amount);
                s.setInt(2, nurseryId);
                s.setInt(3, medicineId);
                return db.ok(s.executeUpdate() == 1);
            } else { //create new
                String sql = "INSERT INTO "+tableName("MedicineStock")+" VALUES(?, ?, ?)";
                PreparedStatement s = con.prepareStatement(sql);
                s.setInt(1, nurseryId);
                s.setInt(2, medicineId);
                s.setInt(3, amount);
                return db.ok(s.executeUpdate() == 1);
            }
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Boolean> medicineExists(int nurseryId, int medicineId) {
        DatabaseResponse<Boolean> db = new DatabaseResponse<>();

        try {
            String sql = "SELECT * FROM "+tableName("MedicineStock")+" WHERE nurseryId = ? AND medicineId = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, nurseryId);
            s.setInt(2, medicineId);
            ResultSet rs = s.executeQuery();

            return db.ok(rs.next());
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Medicine> getMedicine(int nurseryId, int medicineId) {
        DatabaseResponse<Medicine> db = new DatabaseResponse<>();

        try {
            String sql = "SELECT * FROM "+tableName("MedicineStock")+" WHERE nurseryId = ? AND medicineId = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, nurseryId);
            s.setInt(2, medicineId);
            ResultSet rs = s.executeQuery();

            if (rs.next()) return db.ok(new Medicine(rs.getInt(2), rs.getInt(3)));
            else return db.error("Medicine not found in stock.");
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<List<Patient>> getPatients(int nurseryId) {
        DatabaseResponse<List<Patient>> db = new DatabaseResponse<>();

        try {
            ArrayList<Patient> pats = new ArrayList<>();

            String sql = "SELECT * FROM "+tableName("Patients")+" WHERE nurseryId = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, nurseryId);
            ResultSet rs = s.executeQuery();

            while (rs.next())
                pats.add(new Patient(rs.getInt(1), nurseryId));

            return db.ok(pats);
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Patient> getPatient(int patientId) {
        DatabaseResponse<Patient> db = new DatabaseResponse<>();

        try {
            String sql = "SELECT * FROM "+tableName("Patients")+" WHERE id = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, patientId);
            ResultSet rs = s.executeQuery();

            if (rs.next()) return db.ok(new Patient(patientId, rs.getInt(2)));
            else return db.error("Patient not found in any nursery.");
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    /**
     * returns -1 if none available
     */
    public DatabaseResponse<Integer> addPatient(int patientId, String nurseryType) {
        DatabaseResponse<Integer> db = new DatabaseResponse<>();

        try {
            DatabaseResponse<Integer> candidate = findAvailableNursery(nurseryType);
            if (candidate.error) return db.error(candidate.errorMessage);
            if (candidate.response == -1) return db.ok(-1);

            String sql = "INSERT INTO "+tableName("Patients")+" VALUES(?, ?)";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, patientId);
            s.setInt(2, candidate.response);

            return db.ok(s.executeUpdate() == 1 ? candidate.response : -1);
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Boolean> removePatient(int patientId) {
        DatabaseResponse<Boolean> db = new DatabaseResponse<>();

        try {
            String sql = "DELETE FROM "+tableName("Patients")+" WHERE id = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, patientId);

            return db.ok(s.executeUpdate() == 1);
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    /**
     * returns -1 if none available
     */
    public DatabaseResponse<Integer> findAvailableNursery(String type) {
        DatabaseResponse<Integer> db = new DatabaseResponse<>();

        try {
            String sql = "SELECT * FROM "+tableName("Nurseries")+" WHERE specialty = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setString(1, type);
            ResultSet rs = s.executeQuery();

            DatabaseResponse<Integer> count;
            while (rs.next()) {
                count = getPatientCount(rs.getInt(1));
                if (count.error) return db.error(count.errorMessage);
                if (count.response < rs.getInt(4)) return db.ok(rs.getInt(1));
            }

            return db.ok(-1);
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Integer> getPatientCount(int nurseryId) {
        DatabaseResponse<Integer> db = new DatabaseResponse<>();

        try {
            String sql = "SELECT * FROM "+tableName("Patients")+" WHERE nurseryId = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, nurseryId);
            ResultSet rs = s.executeQuery();

            int c = 0;
            while (rs.next()) c++;

            return db.ok(c);
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<List<Nurse>> getNursesInNursery(int nurseryId) {
        DatabaseResponse<List<Nurse>> db = new DatabaseResponse<>();

        try {
            ArrayList<Nurse> nurses = new ArrayList<>();

            String sql = "SELECT * FROM "+tableName("Schedule")+" WHERE nurseryId = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, nurseryId);
            ResultSet rs = s.executeQuery();

            while (rs.next())
                nurses.add(new Nurse(rs.getInt(4), rs.getInt(1)));
            return db.ok(nurses);
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Nurse> getNurse(int nurseId, @Nullable String idCard) {
        DatabaseResponse<Nurse> db = new DatabaseResponse<>();

        if(idCard!=null){
            try {
                String sql = "SELECT * FROM "+tableName("Nurse")+" WHERE idCard = ?";
                PreparedStatement s = con.prepareStatement(sql);
                s.setString(1, idCard);
                ResultSet rs = s.executeQuery();

                if (rs.next())
                    return db.ok(new Nurse(rs.getInt(1), rs.getInt(2),rs.getString(3)));
                else return db.error("Nurse not found in any nursery.");
            } catch (SQLException e) {
                return db.error(e);
            }
        }

        try {
            String sql = "SELECT * FROM "+tableName("Nurse")+" WHERE nurseId = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, nurseId);
            ResultSet rs = s.executeQuery();

            if (rs.next())
                return db.ok(new Nurse(rs.getInt(1), rs.getInt(2)));
            else return db.error("Nurse not found in any nursery.");
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Boolean> nurseExists(int nurseId, String idCard) {
        DatabaseResponse<Boolean> db = new DatabaseResponse<>();

        if (idCard != null) {
            try {
                String sql = "SELECT * FROM "+tableName("Nurse")+" WHERE idCard = ?";
                PreparedStatement s = con.prepareStatement(sql);
                s.setString(1, idCard);
                ResultSet rs = s.executeQuery();

                return db.ok(rs.next());
            } catch (SQLException e) {
                return db.error(e);
            }
        } else {

            try {
                String sql = "SELECT * FROM "+tableName("Nurse")+" WHERE nurseId = ?";
                PreparedStatement s = con.prepareStatement(sql);
                s.setInt(1, nurseId);
                ResultSet rs = s.executeQuery();

                return db.ok(rs.next());
            } catch (SQLException e) {
                return db.error(e);
            }
        }
    }

    public DatabaseResponse<Boolean> patientExists(int patientId) {
        DatabaseResponse<Boolean> db = new DatabaseResponse<>();

        try {
            String sql = "SELECT * FROM "+tableName("Patients")+" WHERE id = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, patientId);
            ResultSet rs = s.executeQuery();

            return db.ok(rs.next());
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Boolean> editNursery(Nursery nursery) {
        DatabaseResponse<Boolean> db = new DatabaseResponse<>();

        try {
            String sql = "UPDATE "+tableName("Nurseries")+" SET name = ?, specialty = ?, beds = ?," +
                    "location = ?, responsible = ? WHERE id = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setString(1, nursery.name);
            s.setString(2, nursery.specialty);
            s.setInt(3, nursery.beds);
            s.setString(4, nursery.location);
            s.setInt(5, nursery.responsible);
            s.setInt(6, nursery.id);

            return db.ok(s.executeUpdate() == 1);
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Boolean> nurseryExists(int nurseryId) {
        DatabaseResponse<Boolean> db = new DatabaseResponse<>();

        try {
            String sql = "SELECT * FROM "+tableName("Nurseries")+" WHERE id = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, nurseryId);
            ResultSet rs = s.executeQuery();

            return db.ok(rs.next());
        } catch (SQLException e) {
            return db.error(e);
        }
    }


    public DatabaseResponse<List<Nursery>> getNurseries() {
        DatabaseResponse<List<Nursery>> db = new DatabaseResponse<>();

        try {
            ArrayList<Nursery> ns = new ArrayList<>();

            String sql = "SELECT * FROM "+tableName("Nurseries");
            PreparedStatement s = con.prepareStatement(sql);
            ResultSet rs = s.executeQuery();

            while (rs.next())
                ns.add(new Nursery(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getInt(4), rs.getString(5), rs.getInt(6)));

            return db.ok(ns);
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Nursery> getNursery(int nurseryId) {
        DatabaseResponse<Nursery> db = new DatabaseResponse<>();

        try {
            String sql = "SELECT * FROM "+tableName("Nurseries")+" WHERE id = ?";

            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, nurseryId);
            ResultSet rs = s.executeQuery();

            if (rs.next()) return db.ok(new Nursery(nurseryId, rs.getString(2), rs.getString(3),
                    rs.getInt(4), rs.getString(5), rs.getInt(6)));
            else return db.error("Nursery not found.");
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Set<String>> getNurseryTypes() {
        DatabaseResponse<Set<String>> db = new DatabaseResponse<>();

        try {
            HashSet<String> types = new HashSet<>();

            String sql = "SELECT * FROM "+tableName("Nurseries");
            PreparedStatement s = con.prepareStatement(sql);
            ResultSet rs = s.executeQuery();

            while (rs.next())
                types.add(rs.getString(3));

            return db.ok(types);
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    //should delete related entries from other tables?
    public DatabaseResponse<Boolean> removeNursery(int nurseryId) {
        DatabaseResponse<Boolean> db = new DatabaseResponse<>();

        try {
            String sql = "DELETE FROM "+tableName("Nurseries")+" WHERE id = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, nurseryId);

            return db.ok(s.executeUpdate() == 1);
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Boolean> addNursery(Nursery nursery) {
        DatabaseResponse<Boolean> db = new DatabaseResponse<>();

        try {
            String sql = "INSERT INTO "+tableName("Nurseries")+" VALUES(?, ?, ?, ?, ?, ?)";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, nursery.id);
            s.setString(2, nursery.name);
            s.setString(3, nursery.specialty);
            s.setInt(4, nursery.beds);
            s.setString(5, nursery.location);
            s.setInt(6, nursery.responsible);

            return db.ok(s.executeUpdate() == 1);
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Schedule> getNurseSchedule(int nurseId) {
        DatabaseResponse<Schedule> db = new DatabaseResponse<>();

        try {
            String sql = "SELECT * FROM "+tableName("Schedule")+" WHERE nurseId = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, nurseId);
            ResultSet rs = s.executeQuery();

            if (rs.next())
                return db.ok(new Schedule(rs.getInt(1), rs.getTimestamp(2), rs.getTimestamp(3), rs.getInt(4),
                        rs.getInt(5)));

            else return db.error("Schedule not found in that nurse.");
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Boolean> addNurse(Nurse nurse) {
        DatabaseResponse<Boolean> db = new DatabaseResponse<>();

        if (nurse.idCard != null) {
            try {
                String sql = "INSERT INTO " + tableName("Nurse") + " VALUES(?,?,?)";
                PreparedStatement s = con.prepareStatement(sql);
                s.setInt(1, nurse.id);
                s.setInt(2, 0);
                s.setString(3, nurse.idCard);
                return db.ok(s.executeUpdate() == 1);
            } catch (SQLException e) {
                return db.error(e);
            }
        } else {

            try {
                String sql = "INSERT INTO " + tableName("Nurse") + " (nurseId) VALUES(?)";
                PreparedStatement s = con.prepareStatement(sql);
                s.setInt(1, nurse.id);


                return db.ok(s.executeUpdate() == 1);
            } catch (SQLException e) {
                return db.error(e);
            }
        }
    }


    public DatabaseResponse<Boolean> addNurseToNursery(Nursery nursery, Nurse nurse, Schedule schedule) {
        DatabaseResponse<Boolean> db = new DatabaseResponse<>();
        try {
            String sql = "INSERT INTO "+tableName("Schedule")+" VALUES(?, ?, ?, ?, ?)";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, schedule.id);
            s.setTimestamp(2, schedule.timeStart);
            s.setTimestamp(3, schedule.timeExit);
            s.setInt(4, nurse.id);
            s.setInt(5, nursery.id);

            boolean b = s.executeUpdate() == 1;

            String sql2 = "UPDATE "+tableName("Nurse")+" SET scheduleId= ? WHERE nurseId=" + nurse.id;
            PreparedStatement s2 = con.prepareStatement(sql2);
            s2.setInt(1, schedule.id);

            boolean b2 = s2.executeUpdate() == 1;
            System.out.println(b+"/"+b2);
            return db.ok(b && b2);
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Schedule> getSchedule(int scheduleId) {
        DatabaseResponse<Schedule> db = new DatabaseResponse<>();

        try {
            String sql = "SELECT * FROM "+tableName("Schedule")+" WHERE scheduleId = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, scheduleId);
            ResultSet rs = s.executeQuery();

            if (rs.next())
                return db.ok(new Schedule(rs.getInt(1), rs.getTimestamp(2), rs.getTimestamp(3), rs.getInt(4),
                        rs.getInt(5)));

            else return db.error("Schedule not found");
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Boolean> addSchedule(Schedule schedule) {
        DatabaseResponse<Boolean> db = new DatabaseResponse<>();

        try {
            String sql = "INSERT INTO "+tableName("Schedule")+" VALUES(?, ?, ?, ?, ?)";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1, schedule.id);
            s.setTimestamp(2, schedule.timeStart);
            s.setTimestamp(3, schedule.timeExit);
            s.setInt(4, schedule.nurseId);
            s.setInt(5, schedule.nurseryId);


            return db.ok(s.executeUpdate() == 1);
        } catch (SQLException e) {
            return db.error(e);
        }
    }

    public DatabaseResponse<Boolean> scheduleExists(int scheduleId) {

        DatabaseResponse<Boolean> db = new DatabaseResponse<>();

        try {
            String sql = "SELECT * FROM "+tableName("Schedule")+" WHERE scheduleId = ?";
            PreparedStatement s = con.prepareStatement(sql);
            s.setInt(1,scheduleId);
            ResultSet rs = s.executeQuery();

            return db.ok(rs.next());
        } catch (SQLException e) {
            return db.error(e);
        }
    }


    public DatabaseResponse<List<Nurse>> getAvailableNurses() {
        DatabaseResponse<List<Nurse>> db = new DatabaseResponse<>();
        ArrayList<Nurse> allNurses = new ArrayList<>();
        ArrayList<Nurse> availableNurses = new ArrayList<>();

        try {


            String sql = "SELECT * FROM "+tableName("Nurse");
            PreparedStatement s = con.prepareStatement(sql);
            ResultSet rs = s.executeQuery();

            while (rs.next()) {
                allNurses.add(new Nurse(rs.getInt(1), rs.getInt(2),rs.getString(3)));
            }
            rs.close();

            for (Nurse nurse : allNurses) {
                if (nurse.scheduleId == 0) {
                    availableNurses.add(nurse);
                }

                String sql2 = "SELECT * FROM "+tableName("Schedule")+" WHERE scheduleId=?";
                PreparedStatement s2 = con.prepareStatement(sql2);
                s2.setInt(1, nurse.scheduleId);
                ResultSet rs2 = s2.executeQuery();

                if (rs2.next()) {
                    Schedule nurseSchedule = new Schedule(rs2.getInt(1), rs2.getTimestamp(2), rs2.getTimestamp(3), rs2.getInt(4),
                            rs2.getInt(5));
                    if (nurseSchedule.currentMillisAfterExit() >= 43200000 ) {
                        availableNurses.add(nurse);
                    }
                }

            }
            return db.ok(availableNurses);
        } catch (SQLException e) {
            return db.error(e);
        }

    }

    public void execute(String sql) {
        _execute(sql, false);
    }

    public void execute(String sql, boolean silent) {
        _execute(sql, silent);
    }

    private void _execute(String sql, boolean silent) {
        try {
            Statement s = con.createStatement();
            if (s.execute(sql) && !silent) System.out.println("Execute -> success. Returned a ResultSet.");
            else if (!silent) System.out.println("Execute -> success. Returned update count or no results.");
        } catch (SQLException e) {
            if (!silent) System.out.println("Execute -> failed.");
            e.printStackTrace();
        }
    }

    public ArrayList<String> getAllDatabases() {
        try {
            ArrayList<String> list = new ArrayList<>();
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery("SHOW DATABASES");
            while (rs.next())
                list.add(rs.getString("Database"));
            rs.close();
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<String> getAllTables() {
        try {
            ArrayList<String> list = new ArrayList<>();
            ResultSet rs = con.getMetaData().getTables(con.getCatalog(), null, "%", new String[]{"TABLE"});
            while (rs.next())
                list.add(rs.getString(3));
            rs.close();
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<String> getAllColumns(String table) {
        try {
            ArrayList<String> list = new ArrayList<>();
            Statement s = con.createStatement();
            ResultSetMetaData rs = s.executeQuery("SELECT * FROM " + table).getMetaData();
            for (int i = 1; i <= rs.getColumnCount(); i++)
                list.add(rs.getColumnName(i));
            s.close();
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


}

