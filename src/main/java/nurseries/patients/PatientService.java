package nurseries.patients;

import nurseries.database.Database;
import nurseries.database.DatabaseResponse;
import java.util.List;

public class PatientService {

    private Database db;

    public PatientService() {
        this.db = Database.getInstance();
    }

    public DatabaseResponse<List<Patient>> getPatients(int nurseryId) {
        return db.getPatients(nurseryId);
    }

    public DatabaseResponse<Patient> getPatient(int patientId) {
        return db.getPatient(patientId);
    }

    public DatabaseResponse<Integer> allocatePatient(int patientId, String nurseryType) {
        return db.addPatient(patientId, nurseryType);
    }

    public DatabaseResponse<Boolean> patientExists(int patientId) {
        return db.patientExists(patientId);
    }

    public DatabaseResponse<Boolean> dischargePatient(int patientId) {
        return db.removePatient(patientId);
    }
}
