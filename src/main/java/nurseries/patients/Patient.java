package nurseries.patients;

import org.json.JSONObject;

public class Patient {
    public int id;
    public int nurseryId;

    public Patient(int id, int nurseryId) {
        this.id = id;
        this.nurseryId = nurseryId;
    }

    public JSONObject toJSON() {
        return new JSONObject().put("id", id).put("nurseryId", nurseryId);
    }

    @Override
    public String toString() {
        return String.format("(patientId: %d, nurseryId: %d)", id, nurseryId);
    }
}
