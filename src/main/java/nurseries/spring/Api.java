package nurseries.spring;

import nurseries.medicine.Medicine;
import nurseries.medicine.MedicineService;
import nurseries.nursery.Nursery;
import nurseries.nursery.NurseryService;
import nurseries.patients.PatientService;
import nurseries.personnel.Nurse;
import nurseries.personnel.NurseService;
import nurseries.personnel.Schedule;
import nurseries.results.Result;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import nurseries.spring.Exceptions.*;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

import static nurseries.spring.Utils.*;

@EnableAutoConfiguration
@RestController
public class Api {

    private NurseryService nurseries;
    private PatientService patients;
    private MedicineService medicine;
    private NurseService nurses;

    private final String KAFKA_HOST = "localhost";
    private final int KAFKA_PORT = 9092;

    //Pedidos Http
    private final static CloseableHttpClient httpClient = HttpClients.createDefault();


    public Api() {
        nurseries = new NurseryService();
        patients = new PatientService();
        nurses = new NurseService();
        medicine = new MedicineService(KAFKA_HOST, KAFKA_PORT);
    }

    /**
     * UC 1
     */
    @RequestMapping(value = "/nurseries", method = RequestMethod.GET)
    public ResponseEntity<String> getNurseries() {

        List<Nursery> list = nurseries.getNurseries().get();

        JSONArray json = new JSONArray();
        for (Nursery n : list) {
            JSONObject item = new JSONObject();
            item.put("id", n.id).put("type", n.specialty).put("name", n.name);
            json.put(item);
        }

        return respond(HttpStatus.OK, json);
    }

    /**
     * UC 2
     */
    @RequestMapping(value = "/nurseries/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> getNursery(@PathVariable("id") String _id) {

        int id = parse(_id, "Nursery id");

        nurseryMustExist(id);

        Nursery n = nurseries.getNursery(id).get();

        return respond(HttpStatus.OK, n.toJSON());
    }

    /**
     * UC 3
     */
    @RequestMapping(value = "/nurseries/types", method = RequestMethod.GET)
    public ResponseEntity<String> getTypes() {

        Set<String> types = nurseries.getTypes().get();

        JSONArray json = new JSONArray();
        for (String t : types)
            json.put(t);

        return respond(HttpStatus.OK, json);
    }

    /**
     * UC 4
     */
    @RequestMapping(value = "/nurseries/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> delNursery(@PathVariable("id") String _id) {

        int id = parse(_id, "Nursery id");

        nurseryMustExist(id);

        boolean result = nurseries.removeNursery(id).get();

        JSONObject item = new JSONObject().put("success", result);

        return respond(HttpStatus.OK, item);
    }

    /**
     * UC 5
     */
    @RequestMapping(value = "/nurseries", method = RequestMethod.POST)
    public ResponseEntity<String> addNursery(@RequestBody Map<String, String> body) {

        fieldsMustExist(body, "name", "type", "beds", "location", "responsible");

        int beds = parse(body.get("beds"), "beds");
        int resp = parse(body.get("responsible"), "responsible");
        int id = generateID();

        boolean result = nurseries.addNursery(
                new Nursery(id, body.get("name"), body.get("type"), beds, body.get("location"), resp)).get();

        JSONObject item = new JSONObject();
        if (result) return respond(HttpStatus.CREATED, item.put("id", id));
        else throw new DatabaseException("Failed to add nursery with id " + id + ".");
    }

    /**
     * UC 6
     */
    @RequestMapping(value = "/nurseries/{id}", method = RequestMethod.PUT)
    public ResponseEntity<String> editNursery(@RequestBody Map<String, String> body, @PathVariable("id") String _id) {

        int id = parse(_id, "Nursery id");

        nurseryMustExist(id);

        Nursery old = nurseries.getNursery(id).get();

        Nursery n = new Nursery(old.id,
                body.containsKey("name") ? body.get("name") : old.name,
                body.containsKey("type") ? body.get("type") : old.specialty,
                body.containsKey("beds") ? parse(body.get("beds"), "beds") : old.beds,
                body.containsKey("location") ? body.get("location") : old.location,
                body.containsKey("responsible") ? parse(body.get("responsible"), "responsible") : old.responsible);

        boolean result = nurseries.editNursery(n).get();

        if (result) return respond(HttpStatus.CREATED, n.toJSON());
        else throw new DatabaseException("Failed to edit nursery with id " + id + ".");
    }

    /**
     * UC 7
     */
    @RequestMapping(value = "/patients", method = RequestMethod.POST)
    public ResponseEntity<String> allocate(@RequestBody Map<String, String> body) {

        fieldsMustExist(body, "id", "type");

        int id = parse(body.get("id"), "id");

        int nurseryId = patients.allocatePatient(id, body.get("type")).get();

        JSONObject item = new JSONObject();
        if (nurseryId != -1) return respond(HttpStatus.OK, item.put("success", true).put("nurseryId", nurseryId));
        else return respond(HttpStatus.OK, item.put("success", false));
    }

    /**
     * UC 8
     */
    @RequestMapping(value = "/patients/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> discharge(@PathVariable("id") String _id) {

        int id = parse(_id, "Patient id");

        patientMustExist(id);

        boolean result = patients.dischargePatient(id).get();

        JSONObject item = new JSONObject();
        item.put("success", result);

        return respond(HttpStatus.OK, item);
    }


    /**
     * UC 9
     */
    @RequestMapping(value = "/nurseries/{id}/nurses", method = RequestMethod.GET)
    public ResponseEntity<String> getNursesInNursery(@PathVariable("id") String _id) {

        int id = parse(_id, "nursery id");

        nurseryMustExist(id);


        List<Nurse> list = nurses.getNursesInNursery(id).get();

        JSONArray json = new JSONArray();
        for (Nurse n : list)
            json.put(n.toJSON());

        return respond(HttpStatus.OK, json);
    }

    /**
     * UC 9.1
     */
    @RequestMapping(value = "/nurses/availablenurses", method = RequestMethod.GET)
    public ResponseEntity<String> getAvailableNurses() {


        List<Nurse> list = nurses.getAvailableNurses().get();

        JSONArray json = new JSONArray();
        for (Nurse n : list)
            json.put(n.toJSON());

        return respond(HttpStatus.OK, json);
    }


    /**
     * UC 9.2
     */
    @RequestMapping(value = "/nurses", method = RequestMethod.GET)
    public ResponseEntity<String> loadNurses() {

        String urlPersonnel = "http://nursery-env.q23hmnaug9.eu-west-2.elasticbeanstalk.com";
        String url = "http://localhost:8083";
        Result getNursesResult = null;
        try {
            HttpGet request = new HttpGet(urlPersonnel + "/employees");
            try (CloseableHttpResponse response = httpClient.execute(request)) {

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode != 200) {
                    throw new RuntimeException("Failed with HTTP error code : " + statusCode);
                }
                HttpEntity httpEntity = response.getEntity();
                String apiOutPut = EntityUtils.toString(httpEntity);

                JSONObject jsonObject = new JSONObject(apiOutPut);
                int employeeNumber = jsonObject.getJSONArray("nurses").length();

                List<Nurse> list = new ArrayList<>();

                for (int i = 0; i < employeeNumber; i++) {
                    if (jsonObject.getJSONArray("nurses").getJSONObject(i).get("kind").equals("nurse")) {
                        String idCard = (String) jsonObject.getJSONArray("nurses").getJSONObject(i).get("idCard");


                        if (nurses.nurseExists(0, idCard).response) {
                            Nurse n = nurses.getNurse(0, idCard).response;
                            list.add(n);
                        } else {
                            nurses.addNurse(new Nurse(generateID(), 0, idCard));
                            Nurse n = nurses.getNurse(0, idCard).response;
                            list.add(n);
                        }


                    }

                }

                JSONArray json = new JSONArray();
                for (Nurse n : list)
                    json.put(n.toJSON());


                return respond(HttpStatus.OK, json);
            }
        } catch (IOException e) {
            System.out.println("aaa");
            return null;
        }


      /*  List<Nurse> list = nurses.getAvailableNurses().get();

        JSONArray json = new JSONArray();
        for (Nurse n: list)
            json.put(n.toJSON());

        return respond(HttpStatus.OK, json);*/

    }


    /**
     * UC 9.2

     @RequestMapping(value = "/nurses", method = RequestMethod.POST)
     public ResponseEntity<String> createNurse() {


     int id = generateID();

     boolean result = nurses.addNurse(new Nurse(id)).get();

     JSONObject item = new JSONObject();
     if (result) return respond(HttpStatus.CREATED, item.put("id", id));
     else throw new DatabaseException("Failed to add nurse with id " + id + ".");
     }*/


    /**
     * UC 10
     */
    @RequestMapping(value = "/nurseries/{id}/nurse", method = RequestMethod.POST)
    public ResponseEntity<String> addNurseToNursery(@RequestBody Map<String, String> body, @PathVariable("id") String _id) {

        fieldsMustExist(body, "scheduleType");

        int nurseryId = parse(_id, "Nursery id");

        String idCard= body.get("idCard");

        nurseryMustExist(nurseryId);

        int scheduleId = generateID(), nurseId = generateID();

        Nurse nurse = nurses.getNurse(nurseId,idCard).response;



        Schedule schedule = generateSchedule(scheduleId, body.get("scheduleType"), nurseId, nurseryId);
        Nursery nursery = nurseries.getNursery(nurseryId).get();

        if(!nurses.nurseExists(nurse.id,nurse.idCard).get()){
            throw new DatabaseException("Nurse with id Card" + idCard + " not found .");
        }

        if (!nurses.addSchedule(schedule).get())
            throw new DatabaseException("Failed to add schedule with id " + scheduleId + " to nurse " + nurseId + " and nursery " + nurseryId + ".");

        nurses.addNurseToNursery(nursery, nurse, schedule);

        JSONObject item = new JSONObject();
        return respond(HttpStatus.CREATED, item.put("nurse with Id card", idCard).put(" allocated with scheduleId", scheduleId));
    }

    /**
     * UC 11
     */
    @RequestMapping(value = "/nurseries/{id}/medicines", method = RequestMethod.GET)
    public ResponseEntity<String> getStock(@PathVariable("id") String _id) {

        int id = parse(_id, "Nursery id");

        nurseryMustExist(id);

        List<Medicine> list = medicine.getStock(id).get();

        JSONArray json = new JSONArray();
        for (Medicine m : list) {
            if (m.stock == 0) continue;
            json.put(m.toJSON());
        }

        return respond(HttpStatus.OK, json);
    }


    /**
     * UC 12
     */
    @RequestMapping(value = "/nurseries/{id}/medicines", method = RequestMethod.POST)
    public ResponseEntity<String> requestStock(@RequestBody Map<String, String> body, @PathVariable("id") String _id) {

        int id = parse(_id, "Nursery id");

        nurseryMustExist(id);

        fieldsMustExist(body, "medicineId", "amount");

        int medicineId = parse(body.get("medicineId"), "medicineId");
        int amount = parse(body.get("amount"), "amount");

        if (amount < 1) throw new NegativeIntException("amount");

        medicine.request(id, medicineId, amount);

        JSONObject json = new JSONObject().put("topic", medicine.requestTopic);

        return respond(HttpStatus.CREATED, json);
    }

    /**
     * UC 13
     */
    @RequestMapping(value = "/nurseries/{id}/medicines", method = RequestMethod.PUT)
    public ResponseEntity<String> consumeStock(@RequestBody Map<String, String> body, @PathVariable("id") String _id) {

        int id = parse(_id, "Nursery id");

        nurseryMustExist(id);

        fieldsMustExist(body, "medicineId", "amount");

        int medicineId = parse(body.get("medicineId"), "medicineId");
        int amount = parse(body.get("amount"), "amount");

        if (amount < 1) throw new NegativeIntException("amount");

        medicineMustExist(id, medicineId);

        Medicine m = medicine.getMedicine(id, medicineId).get();

        boolean result = medicine.remove(id, medicineId, amount).get();


        JSONObject item = new JSONObject();
        item.put("success", result);
        if (result) return respond(HttpStatus.OK, item.put("remaining", m.stock - amount));
        else return respond(HttpStatus.OK, item.put("remaining", m.stock));
    }

    /**
     * UC 17
     */
    @RequestMapping(value = "/medicines/notifications", method = RequestMethod.POST)
    public ResponseEntity<String> removeStock(@RequestBody Map<String, String> body) {

        fieldsMustExist(body, "orderId");

        System.out.println("[Notification] Order with id '" + body.get("orderId") + "' was fulfilled.");

        JSONObject json = new JSONObject();
        return respond(HttpStatus.OK, json.put("success", true));
    }

    private void nurseryMustExist(int id) {
        if (!nurseries.nurseryExists(id).get()) throw new MustExistException("nursery", id);
    }

    private void patientMustExist(int id) {
        if (!patients.patientExists(id).get()) throw new MustExistException("patient", id);
    }

    private void medicineMustExist(int nurseryId, int medicineId) {
        if (!medicine.medicineExists(nurseryId, medicineId).get())
            throw new MustExistException("medicine", medicineId, "nursery", nurseryId);
    }

    private void fieldsMustExist(Map<String, String> body, String... fieldNames) {
        for (String name : fieldNames) {
            if (!body.containsKey(name)) throw new MissingFieldException(name);
        }
    }
}