package nurseries.spring;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.*;
import nurseries.spring.Exceptions.*;
import static nurseries.spring.Utils.error;

@RestController
@ControllerAdvice
public class ErrorHandler implements ErrorController {

    @RequestMapping(value = "/error")
    public ResponseEntity<String> badUrl() {
        return error(HttpStatus.NOT_FOUND, "Unexpected request path.");
    }

    @Override
    public String getErrorPath() { return "/error"; }

    @ExceptionHandler(IntParseException.class)
    public ResponseEntity<String> intEx(IntParseException ex) {
        return error(HttpStatus.BAD_REQUEST, ex.input+" must be an integer.");
    }

    @ExceptionHandler(MissingFieldException.class)
    public ResponseEntity<String> missEx(MissingFieldException ex) {
        return error(HttpStatus.BAD_REQUEST, "'"+ex.name+"' is a required field.");
    }

    @ExceptionHandler(NegativeIntException.class)
    public ResponseEntity<String> negEx(NegativeIntException ex) {
        return error(HttpStatus.BAD_REQUEST, "'"+ex.name+"' must be a positive integer.");
    }

    @ExceptionHandler(MustExistException.class)
    public ResponseEntity<String> existEx(MustExistException ex) {
        if (ex.origin==null) return error(HttpStatus.NOT_FOUND, "A "+ex.name+" with id "+ex.id+" does not exist.");
        else return error(HttpStatus.NOT_FOUND, "A "+ex.name+" with id "+ex.id+" does not exist in "+ex.origin+" "+ex.originId+".");
    }

    @ExceptionHandler(ScheduleException.class)
    public ResponseEntity<String> shEx(ScheduleException ex) {
        return error(HttpStatus.BAD_REQUEST, "Schedule creation failed because "+ex.reason+".");
    }

    @ExceptionHandler(DatabaseException.class)
    public ResponseEntity<String> dbEx(DatabaseException ex) {
        System.out.println("[Database error] "+ex.reason);
        return error(HttpStatus.INTERNAL_SERVER_ERROR, "An internal error occurred.");
    }

    @ExceptionHandler(KafkaException.class)
    public ResponseEntity<String> kafkaEx(KafkaException ex) {
        System.out.println("[Kafka error] "+ex.reason);
        return error(HttpStatus.INTERNAL_SERVER_ERROR, "An internal error occurred.");
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<String> httpEx(HttpRequestMethodNotSupportedException ex) {
        return error(HttpStatus.METHOD_NOT_ALLOWED, "That request method is not supported for this path.");
    }

}
