package nurseries.spring;

import nurseries.personnel.Schedule;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import nurseries.spring.Exceptions.*;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

public class Utils {

    private static Random random = new Random();

    private Utils() {};

    public static ResponseEntity<String> respond(HttpStatus status, JSONObject body) {
        return ResponseEntity.status(status).body(body.toString());
    }

    public static ResponseEntity<String> respond(HttpStatus status, JSONArray body) {
        return ResponseEntity.status(status).body(body.toString());
    }

    public static ResponseEntity<String> error(HttpStatus status, String error) {
        JSONObject json = new JSONObject().put("status",status.value()).put("error",error);
        return respond(status, json);
    }

    public static int parse(String value, String varName) {
        try { return Integer.parseInt(value); }
        catch (NumberFormatException e) { throw new IntParseException(varName); }
    }

    public static int generateID() {
        return random.nextInt(Integer.MAX_VALUE)+1;
    }

    public static Schedule generateSchedule(int scheduleId,String type,int nurseId,int nurseryId){

        if(type.equalsIgnoreCase("morning")){

            LocalDateTime timeS= LocalDate.now().plus(1, ChronoUnit.DAYS).atTime(6,0);
            LocalDateTime timeE= LocalDate.now().plus(1, ChronoUnit.DAYS).atTime(14,0);
            Timestamp timeStart=Timestamp.valueOf(timeS);
            Timestamp timeExit= Timestamp.valueOf(timeE);

            return new Schedule(scheduleId,timeStart,timeExit,nurseId,nurseryId);
        }
        if(type.equalsIgnoreCase("afternoon")){

            LocalDateTime timeS= LocalDate.now().plus(1, ChronoUnit.DAYS).atTime(14,0);
            LocalDateTime timeE= LocalDate.now().plus(1, ChronoUnit.DAYS).atTime(22,0);
            Timestamp timeStart=Timestamp.valueOf(timeS);
            Timestamp timeExit= Timestamp.valueOf(timeE);

            return new Schedule(scheduleId,timeStart,timeExit,nurseId,nurseryId);
        }
        if(type.equalsIgnoreCase("night")||type.equalsIgnoreCase("evening")){

            LocalDateTime timeS= LocalDate.now().plus(1, ChronoUnit.DAYS).atTime(22,0);
            LocalDateTime timeE= LocalDate.now().plus(2, ChronoUnit.DAYS).atTime(6,0);
            Timestamp timeStart=Timestamp.valueOf(timeS);
            Timestamp timeExit= Timestamp.valueOf(timeE);

            return new Schedule(scheduleId,timeStart,timeExit,nurseId,nurseryId);
        }


        return null;
    }


    /**
     * Returns the equivalent hours in milliseconds.
     */
    public static long hours(int h) {
        return h*60*60*1000;
    }

    /**
     * Returns the equivalent minutes in milliseconds.
     */
    public static long minutes(int m) {
        return m*60*1000;
    }
}
