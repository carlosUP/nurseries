package nurseries.spring;

public class Exceptions {

    private Exceptions() {};

    public static class IntParseException extends RuntimeException {
        public String input;
        public IntParseException(String input) { this.input = input; }
    }

    public static class MissingFieldException extends RuntimeException {
        public String name;
        public MissingFieldException(String name) { this.name = name; }
    }

    public static class NegativeIntException extends RuntimeException {
        public String name;
        public NegativeIntException(String name) { this.name = name; }
    }

    public static class MustExistException extends RuntimeException {
        public String name; public int id;
        public String origin; public int originId;
        public MustExistException(String name, int id) { this.name = name; this.id = id; }
        public MustExistException(String name, int id, String origin, int originId) {
            this.name = name; this.id = id; this.origin = origin; this.originId = originId;
        }
    }

    public static class ScheduleException extends RuntimeException {
        public String reason;
        public ScheduleException(String reason) { this.reason = reason; }
    }

    public static class DatabaseException extends RuntimeException {
        public String reason;
        public DatabaseException(String reason) { this.reason = reason; }
    }

    public static class KafkaException extends RuntimeException {
        public String reason;
        public KafkaException(String reason) { this.reason = reason; }
    }
}
