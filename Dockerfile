FROM maven:3-jdk-8-alpine

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn package

EXPOSE 8081
CMD [ "sh", "-c", "mvn -Dserver.port=8081 spring-boot:run" ]
