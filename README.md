# nurseries
[![pipeline status](https://gitlab.com/carlosUP/nurseries/badges/master/pipeline.svg)](https://gitlab.com/carlosUP/nurseries/commits/master)
[![pipeline status](https://gitlab.com/carlosUP/nurseries/badges/master/pipeline.svg)](https://gitlab.com/carlosUP/nurseries/commits/master)

Componente das nurseries do projeto.

- Feito em Java
- Comunica com database do AWS
- Recebe e envia mensagens ao servidor kafka principal
    - Pedidos internos e externos são todos feitos ao servidor kafka
	
[Docs aqui](https://docs.google.com/document/d/1TijuR6IPVaA2hcSMEyveZxxMNCsZtb9_dN702ARU4YA).

![image](https://i.imgur.com/f7QtyWK.jpg)